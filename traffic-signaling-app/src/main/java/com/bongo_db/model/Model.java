package com.bongo_db.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Map;


@Data
@Builder
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class Model {
    Metadata metadata;
    Map<String, Street> streetsByName;
    Map<Integer, Intersection> intersectionsById;
    List<Car> cars;

    public void validate() {
        if (metadata.getCarsNumber() != cars.size() ||
                metadata.getStreetsNumber() != streetsByName.size() ||
                metadata.getIntersectionsNumber() != intersectionsById.size()
        ) {
            throw new IllegalStateException("Model validation failed. Incorrect data or input file load process!");
        }
    }
}
