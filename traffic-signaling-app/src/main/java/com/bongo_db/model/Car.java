package com.bongo_db.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.ListIterator;

/**
 * path of car is always valid
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Car{
    final int streetsToTravelNumber;
    final List<Street> streetsToTravel; //car starts on the end of first street

    @Builder
    public Car(int streetsToTravelNumber, List<Street> streetsToTravel) {
        this.streetsToTravelNumber = streetsToTravelNumber;
        this.streetsToTravel = streetsToTravel;
    }

    Street currentStreet;
    int currentStreetPosition;
    int timeWhenFinishedTravel;

    public void initForSimulation(){
        currentStreet = streetsToTravel.stream().findFirst().orElseThrow(() -> new IllegalStateException("No streets for car!"));
        currentStreet.addToCarsWaitingOnTrafficLight(this);
        currentStreetPosition = 0;
        timeWhenFinishedTravel = 0;
    }

    public boolean hasFinishedTravel() {
        return getTimeWhenFinishedTravel() > 0;
    }
    public void rideToNextStreet() {
        ListIterator<Street> listIterator = streetsToTravel.listIterator(streetsToTravel.indexOf(currentStreet)+1);
        if(listIterator.hasNext()){
            Street next = listIterator.next();
            currentStreet = next;
            currentStreet.addNewCrossingCar(this);
            currentStreetPosition = 0;
        } else {
            throw new IllegalStateException("Car tried to ride further than last street: "+this);
        }
    }

    public void updatePosition(){
        currentStreetPosition++;
    }

    public void finishCurrentStreet (int second){
        if(isOnLastStreet()){
            timeWhenFinishedTravel = second;
        } else {
            currentStreet.addToCarsWaitingOnTrafficLight(this);
        }
    }

    public boolean isOnLastStreet() {
        return currentStreet == streetsToTravel.get(streetsToTravel.size()-1);
    }

    public boolean hasFinishedCurrentStreet() {
        return currentStreetPosition == currentStreet.getTimeToTravel();
    }


}
