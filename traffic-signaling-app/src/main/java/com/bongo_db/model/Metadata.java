package com.bongo_db.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class Metadata {
    int maxTimeInSeconds;
    int intersectionsNumber;
    int streetsNumber;
    int carsNumber;
    int bonusForDestinationReached; //before time defined in 'maxTimeInSeconds'
}
