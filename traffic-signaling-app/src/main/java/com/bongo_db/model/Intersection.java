package com.bongo_db.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Intersection {
    @EqualsAndHashCode.Include
    final int id;
    List<Street> incomingStreets;
    List<Street> outgoingStreets;
    TrafficLightsSchedule schedule;

    public void addIncomingStreet(Street street) {
        incomingStreets.add(street);
    }

    public void addOutgoingStreet(Street street) {
        outgoingStreets.add(street);
    }

    @Override
    public String toString() {
        return "Intersection{" +
                "id=" + id +
                ", incomingStreets=" + incomingStreets +
                ", outgoingStreets=" + outgoingStreets +
                '}';
    }
}
