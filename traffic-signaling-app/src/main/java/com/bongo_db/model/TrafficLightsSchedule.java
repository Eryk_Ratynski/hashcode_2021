package com.bongo_db.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.util.LinkedHashMap;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TrafficLightsSchedule {
    @EqualsAndHashCode.Include
    final Intersection intersection;
    /**
     * Type is LinkedHashMap, because we NEED order, we can't use unordered maps
     * green light runs for first street for time (seconds) defined in it's integer value, then second street, etc. Then cycle repeats.
     */
    final LinkedHashMap<Street, Integer> greenLightTimeSchedule;

    @Builder
    public TrafficLightsSchedule(Intersection intersection, LinkedHashMap<Street, Integer> greenLightTimeSchedule) {
        this.intersection = intersection;
        this.greenLightTimeSchedule = greenLightTimeSchedule;
    }

    @Override
    public String toString() {
        return "TrafficLightSchedule{" +
                "intersection=" + intersection.getId() +
                ", greenLightTimeSchedule=" + greenLightTimeSchedule +
                '}';
    }

    int greenLightCycleLength;
    Street streetWithGreenLight;
    boolean anyScheduleAvailable;

    public void initForSimulation(){
        streetWithGreenLight = getFirstStreet();
        greenLightCycleLength = greenLightTimeSchedule.values().stream().mapToInt(Integer::intValue).sum();
        anyScheduleAvailable = greenLightTimeSchedule.values().stream().anyMatch(i -> i > 0);
    }

    public Street getFirstStreet(){
        return greenLightTimeSchedule.entrySet().iterator().next().getKey();
    }

    public void updateStreetWithGreenLightByTime(int currentSecond) {
        if(!anyScheduleAvailable)
            return;
        int relativeTime = currentSecond % greenLightCycleLength;
        for(Street street: greenLightTimeSchedule.keySet()){
            relativeTime -= greenLightTimeSchedule.get(street);
            if(relativeTime < 0){
                streetWithGreenLight = street;
                break;
            }
        }
    }
}