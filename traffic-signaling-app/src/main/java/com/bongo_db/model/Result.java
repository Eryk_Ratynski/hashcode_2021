package com.bongo_db.model;


import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Result {
    List<TrafficLightsSchedule> trafficLightsSchedules;
}
