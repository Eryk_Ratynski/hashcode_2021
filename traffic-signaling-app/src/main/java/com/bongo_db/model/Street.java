package com.bongo_db.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Street {

    final Intersection startIntersection;
    final Intersection endIntersection;
    @EqualsAndHashCode.Include
    final String name;
    final int timeToTravel; //time in seconds to travel whole street

    @Builder
    public Street(Intersection startIntersection, Intersection endIntersection, String name, int timeToTravel) {
        this.startIntersection = startIntersection;
        this.endIntersection = endIntersection;
        this.name = name;
        this.timeToTravel = timeToTravel;
    }

    @Override
    public String toString() {
        return "Street(startIntersectionId=" + startIntersection.getId() +
                ", endIntersectionId=" + endIntersection.getId() +
                ", name=" + this.getName() +
                ", timeToTravel=" + this.getTimeToTravel() + ")";
    }

    Queue<Car> carsWaitingOnTrafficLights = new LinkedBlockingQueue<>();
    List<Car> crossingCars = new LinkedList<>();

    public void initForSimulation(){
        carsWaitingOnTrafficLights = new LinkedBlockingQueue<>();
        crossingCars = new LinkedList<>();
    }

    public void letCarThroughIntersection() {
        Car car = carsWaitingOnTrafficLights.poll();
        if(Objects.isNull(car))
            return;
        car.rideToNextStreet();
    }

    public void addNewCrossingCar(Car car) {
        crossingCars.add(car);
    }

    public void updateCrossingCarsPositions(int second) {
        Iterator<Car> crossingCarsIterator = crossingCars.iterator();
        while(crossingCarsIterator.hasNext()){
            Car currentCar = crossingCarsIterator.next();
            currentCar.updatePosition();
            if(currentCar.hasFinishedCurrentStreet()){
                currentCar.finishCurrentStreet(second);
                crossingCarsIterator.remove();
            }
        }
    }

    public void addToCarsWaitingOnTrafficLight(Car car) {
        carsWaitingOnTrafficLights.add(car);
    }
}
