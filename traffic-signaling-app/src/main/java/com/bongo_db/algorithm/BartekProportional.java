package com.bongo_db.algorithm;

import com.bongo_db.model.Intersection;
import com.bongo_db.model.Model;
import com.bongo_db.model.Street;
import com.bongo_db.model.TrafficLightsSchedule;
import com.google.common.math.LongMath;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BartekProportional implements Algorithm {

    @Override
    public List<TrafficLightsSchedule> solve(Model model, Map<Intersection, TrafficLightsSchedule> initialSchedule) {
        Map<Street, Long> countAllCarsPerStreet = model.getCars().stream()
                .flatMap(car -> car.getStreetsToTravel().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        //it doesn't contain streets with 0 cars

        for(Intersection intersection: initialSchedule.keySet()){
            Long greatestCommonDivisor = null;
            for(Street incomingStreet: intersection.getIncomingStreets()){
                if(Objects.isNull(greatestCommonDivisor)){
                    greatestCommonDivisor = countAllCarsPerStreet.getOrDefault(incomingStreet, 0L);
                } else {
                    long currentCarsCount = countAllCarsPerStreet.getOrDefault(incomingStreet, 0L);
                    greatestCommonDivisor = LongMath.gcd(greatestCommonDivisor, currentCarsCount);
                }
            }
            if(Objects.isNull(greatestCommonDivisor) || greatestCommonDivisor == 0){
                continue;
            }
            LinkedHashMap<Street, Integer> currentSchedule = initialSchedule.get(intersection).getGreenLightTimeSchedule();
            for(Street incomingStreet: intersection.getIncomingStreets()){
                int newTimeSchedule = countAllCarsPerStreet.getOrDefault(incomingStreet, 0L).intValue()/ greatestCommonDivisor.intValue();
                currentSchedule.computeIfPresent(incomingStreet, (street, oldScheduledTime) -> newTimeSchedule);
            }

        }
        return new ArrayList<>(initialSchedule.values());
    }
}
