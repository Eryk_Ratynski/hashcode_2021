package com.bongo_db.algorithm;

import com.bongo_db.model.Intersection;
import com.bongo_db.model.Model;
import com.bongo_db.model.TrafficLightsSchedule;

import java.util.List;
import java.util.Map;

public interface Algorithm {

    /**
     *
     */
    List<TrafficLightsSchedule> solve(Model model, Map<Intersection, TrafficLightsSchedule> initialSchedule);
}
