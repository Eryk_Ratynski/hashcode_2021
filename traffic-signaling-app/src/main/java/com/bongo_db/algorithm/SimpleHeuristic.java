package com.bongo_db.algorithm;

import com.bongo_db.model.Intersection;
import com.bongo_db.model.Model;
import com.bongo_db.model.Street;
import com.bongo_db.model.TrafficLightsSchedule;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class SimpleHeuristic implements Algorithm {

    @Override
    public List<TrafficLightsSchedule> solve(Model model, Map<Intersection, TrafficLightsSchedule> initialSchedule) {
        initialSchedule.values().forEach(trafficLights -> {
            LinkedHashMap<Street, Integer> timeSchedule = trafficLights.getGreenLightTimeSchedule();
            timeSchedule.keySet()
                    .forEach(s -> timeSchedule.put(s, ThreadLocalRandom.current().nextInt(1, model.getMetadata().getMaxTimeInSeconds()/timeSchedule.size())));
        });
        return new ArrayList<>(initialSchedule.values());
    }
}
