package com.bongo_db;

import com.bongo_db.algorithm.BartekProportional;
import com.bongo_db.algorithm.SimpleHeuristic;
import com.bongo_db.model.Model;
import com.bongo_db.model.Result;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class MainApp {

    public static final Map<String, String> filesMap = new LinkedHashMap<>();

    static {
        filesMap.put("a.txt", "a.out");
        filesMap.put("b.txt", "b.out");
        filesMap.put("c.txt", "c.out");
        filesMap.put("d.txt", "d.out");
        filesMap.put("e.txt", "e.out");
        filesMap.put("f.txt", "f.out");
    }

    public static void main(String[] args) throws IOException {
        runSimulationWith(filesMap);
    }

    private static void runSimulationWith(Map<String, String> filesMap) throws IOException {
        Locale.setDefault(Locale.ENGLISH);
        Solver solver = new Solver(new BartekProportional());
        FileLoader fileLoader = new FileLoader();
        long totalScore = 0;
        for (String file : filesMap.keySet()) {
            Model model = fileLoader.loadModelFromFile(file);
            Result result = solver.solve(model);
            long score = JudgingSystem.validateAndCalculateScore(result, model);
            System.out.println(String.format("Dataset: %s, score: %,d", file, score));
            totalScore += score;
        }
        System.out.println(String.format("Total score: %,d", totalScore));
    }
}
