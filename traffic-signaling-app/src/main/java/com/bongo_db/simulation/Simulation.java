package com.bongo_db.simulation;

import com.bongo_db.model.Car;
import com.bongo_db.model.Model;
import com.bongo_db.model.Street;
import com.bongo_db.model.TrafficLightsSchedule;

import java.util.ArrayList;
import java.util.List;

public class Simulation {

    private final int rewardForFinishedTravel;
    private final int simulationTime;

    List<TrafficLightsSchedule> schedules;
    List<Street> streets;
    List<Car> cars;

    public Simulation(Model model, List<TrafficLightsSchedule> schedules) {
        //dla każdej ulicy tworzymy kolejkę samochodów przed światłami oraz kolekcję aktualnie przemieszczających się po niej samochodów
        //dodajemy kazdy samochód do końcowej kolejki w jego pierwszej ulicy (według kolejności z pliku)
        this.schedules = schedules;
        this.streets = new ArrayList<>(model.getStreetsByName().values());
        this.cars = model.getCars();
        this.rewardForFinishedTravel = model.getMetadata().getBonusForDestinationReached();
        this.simulationTime = model.getMetadata().getMaxTimeInSeconds();

        this.schedules.forEach(TrafficLightsSchedule::initForSimulation);
        this.streets.forEach(Street::initForSimulation);
        this.cars.forEach(Car::initForSimulation);
    }

    public void doTurn(int second) {
        //dla każdego skrzyżowania aktualizujemy ulicę, na której jest zielone
        //dla każdej ulicy, na której jest zielone, przepuszczamy samochód z kolejki na końcu ulicy do następnej ulicy
        //dla każdej ulicy aktualizujemy pozycje wszystkich aktualnie przemieszczających się po niej samochodów,
            // potem wybieramy wszystkie samochody, które przejechały całą ulicę i przenosimy je do kolejki przed światłami
                //jeśli przejechana ulica była ostatnią dla samochodu, aktualizujemy punkty
        for(Street street: streets){
            street.updateCrossingCarsPositions(second);
        }
        for(TrafficLightsSchedule schedule: schedules){
            schedule.updateStreetWithGreenLightByTime(second);
            schedule.getStreetWithGreenLight().letCarThroughIntersection();
        }
    }

    public long getTotalScore() {
        long totalScore = 0;
        for(Car car: cars){
            if(car.hasFinishedTravel()){
                int score = rewardForFinishedTravel + simulationTime - car.getTimeWhenFinishedTravel();
                totalScore += score;
            }
        }
        return totalScore;
    }
}
