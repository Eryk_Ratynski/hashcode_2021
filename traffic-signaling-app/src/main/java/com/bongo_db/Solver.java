package com.bongo_db;


import com.bongo_db.algorithm.Algorithm;
import com.bongo_db.model.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solver {

    private final Algorithm algorithm;

    public Solver(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public Result solve(Model model) {
        Map<Intersection, TrafficLightsSchedule> initialSchedule = initSchedule(model);
        List<TrafficLightsSchedule> solved = algorithm.solve(model, initialSchedule);
        return Result.builder()
                .trafficLightsSchedules(solved)
                .build();
    }

    private Map<Intersection, TrafficLightsSchedule> initSchedule(Model model) {
        return model.getIntersectionsById().values().stream()
                .map(this::createTrafficLightScheduleFromIntersection)
                .collect(Collectors.toMap(TrafficLightsSchedule::getIntersection, Function.identity()));
    }

    private TrafficLightsSchedule createTrafficLightScheduleFromIntersection(Intersection intersection) {
        LinkedHashMap<Street, Integer> schedule = new LinkedHashMap<>();
        intersection.getIncomingStreets().forEach(street -> schedule.put(street, 0));

        TrafficLightsSchedule trafficLightsSchedule = TrafficLightsSchedule.builder()
                .intersection(intersection)
                .greenLightTimeSchedule(schedule)
                .build();
        intersection.setSchedule(trafficLightsSchedule);
        return trafficLightsSchedule;
    }
}
