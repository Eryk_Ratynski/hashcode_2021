package com.bongo_db;

import com.bongo_db.model.Model;
import com.bongo_db.model.Result;
import com.bongo_db.simulation.Simulation;

import java.util.stream.IntStream;

public class JudgingSystem {
    public static long validateAndCalculateScore(Result result, Model model) {
        validateResultWithModel(result, model);
        return calculateScore(result, model);
    }

    private static long calculateScore(Result result, Model model) {
        Simulation simulation = new Simulation(model, result.getTrafficLightsSchedules());
        IntStream.range(0, model.getMetadata().getMaxTimeInSeconds()).forEach(simulation::doTurn);
        return simulation.getTotalScore();
    }

    private static void validateResultWithModel(Result result, Model model) {
        if(result.getTrafficLightsSchedules().size() > model.getIntersectionsById().size()){
            throw new IllegalStateException("Invalid schedules number");
        }
    }

}
