package com.bongo_db;

import com.bongo_db.model.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FileLoader {
    public Model loadModelFromFile(String filename) throws IOException {
        File file = getFileBy(filename);
        List<String> lines = readLinesFromFile(file);
        Metadata modelMetadata = parseMetadataFromFirstLine(filename, lines.get(0));
        Map<Integer, Intersection> intersectionsById = new HashMap<>();
        Map<String, Street> streetsByNames = parseStreetsFromLines(lines, intersectionsById, modelMetadata.getStreetsNumber());

        List<Car> cars = parsePathsOfCarsFromLines(lines, streetsByNames, modelMetadata.getStreetsNumber(), modelMetadata.getCarsNumber());

        Model model = Model.builder()
                .metadata(modelMetadata)
                .streetsByName(streetsByNames)
                .intersectionsById(intersectionsById)
                .cars(cars)
                .build();
        model.validate();
        return model;
    }

    private Metadata parseMetadataFromFirstLine(String filename, String firstLine) {
        List<Integer> modelMetadata = Arrays.stream(firstLine.split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        if (modelMetadata.size() > 5) {
            throw new IllegalStateException(String.format("metadata line of file %s has too many numbers!", filename));
        }
        return Metadata.builder()
                .maxTimeInSeconds(modelMetadata.get(0))
                .intersectionsNumber(modelMetadata.get(1))
                .streetsNumber(modelMetadata.get(2))
                .carsNumber(modelMetadata.get(3))
                .bonusForDestinationReached(modelMetadata.get(4))
                .build();
    }

    private Map<String, Street> parseStreetsFromLines(List<String> lines, Map<Integer, Intersection> intersectionsById, int streetsNumber) {
        return lines.stream()
                .skip(1) //skip metadata line
                .limit(streetsNumber)
                .map(line -> parseStreetFromString(line, intersectionsById))
                .collect(Collectors.toMap(Street::getName, Function.identity()));
    }

    private List<Car> parsePathsOfCarsFromLines(List<String> lines, Map<String, Street> streetsByNames, int streetsNumber, int carsNumber) {
        return lines.stream()
                .skip(1 + streetsNumber) //skip metadata line and streets
                .limit(carsNumber)
                .map(line -> parseCarFromString(line, streetsByNames))
                .collect(Collectors.toList());
    }

    private Car parseCarFromString(String line, Map<String, Street> streetsByNames) {
        String[] lineArray = line.split(" ");
        int streetsToTravelNumber = Integer.parseInt(lineArray[0]);
        List<String> streetNames = Arrays.stream(lineArray)
                .skip(1) //skip first line with streets number
                .collect(Collectors.toList());
        List<Street> streets = streetNames.stream()
                .map(streetsByNames::get)
                .collect(Collectors.toList());
        return Car.builder()
                .streetsToTravelNumber(streetsToTravelNumber)
                .streetsToTravel(streets)
                .build();
    }

    private Street parseStreetFromString(String line, Map<Integer, Intersection> intersectionsById) {
        String[] lineArray = line.split(" ");
        List<Integer> streetIntegers = Arrays.stream(lineArray)
                .filter(this::isInteger)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        Integer startIntersectionId = streetIntegers.get(0);
        Integer endIntersectionId = streetIntegers.get(1);
        String name = lineArray[2];
        Integer timeToTravel = streetIntegers.get(2);

        Intersection start = getByIdOrCreateNew(intersectionsById, startIntersectionId);
        Intersection end = getByIdOrCreateNew(intersectionsById, endIntersectionId);

        Street street = Street.builder()
                .startIntersection(start)
                .endIntersection(end)
                .name(name)
                .timeToTravel(timeToTravel)
                .build();
        start.addOutgoingStreet(street);
        end.addIncomingStreet(street);
        return street;
    }

    private Intersection getByIdOrCreateNew(Map<Integer, Intersection> intersectionsById, Integer startIntersectionId) {
        return intersectionsById.computeIfAbsent(startIntersectionId, key -> Intersection.builder()
                .id(key)
                .incomingStreets(new LinkedList<>())
                .outgoingStreets(new LinkedList<>())
                .build()
        );
    }

    private boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private File getFileBy(String filename) {
        return new File(this.getClass().getClassLoader().getResource(filename).getFile());
    }

    private List<String> readLinesFromFile(File file) throws IOException {
        List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);
        if (lines.isEmpty()) {
            throw new IllegalStateException("input file is empty or unable to be read");
        }
        return lines;
    }
}
