package com.bongo_db;

import com.bongo_db.model.*;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class JudgingSystemTest {

    @Test
    void whenBuiltModelAndRunSimulationThenScoreShouldMatchPrecalculatedScore(){
        //given
        Metadata metadata = Metadata.builder()
                .intersectionsNumber(4)
                .streetsNumber(3)
                .maxTimeInSeconds(10)
                .carsNumber(6)
                .bonusForDestinationReached(10)
                .build();

        Map<Integer, Intersection> intersections = new HashMap<>();
        Intersection beforeA = Intersection.builder().id(0).incomingStreets(new LinkedList<>()).outgoingStreets(new LinkedList<>()).build();
        Intersection beforeB = Intersection.builder().id(1).incomingStreets(new LinkedList<>()).outgoingStreets(new LinkedList<>()).build();
        Intersection middle = Intersection.builder().id(2).incomingStreets(new LinkedList<>()).outgoingStreets(new LinkedList<>()).build();
        Intersection afterC = Intersection.builder().id(3).incomingStreets(new LinkedList<>()).outgoingStreets(new LinkedList<>()).build();
        intersections.put(beforeA.getId(), beforeA);
        intersections.put(beforeB.getId(), beforeB);
        intersections.put(middle.getId(), middle);
        intersections.put(afterC.getId(), afterC);

        Map<String, Street> streets = new HashMap<>();

        Street streetA = Street.builder()
                .startIntersection(beforeA)
                .endIntersection(middle)
                .timeToTravel(5)
                .name("A")
                .build();
        streets.put(streetA.getName(), streetA);

        Street streetB = Street.builder()
                .startIntersection(beforeB)
                .endIntersection(middle)
                .timeToTravel(5)
                .name("B")
                .build();
        streets.put(streetB.getName(), streetB);

        Street streetC = Street.builder()
                .startIntersection(middle)
                .endIntersection(afterC)
                .timeToTravel(5)
                .name("C")
                .build();
        streets.put(streetC.getName(), streetC);

        beforeA.addOutgoingStreet(streetA);
        beforeB.addOutgoingStreet(streetB);
        middle.addIncomingStreet(streetA);
        middle.addIncomingStreet(streetB);
        middle.addOutgoingStreet(streetC);
        afterC.addIncomingStreet(streetC);

        List<Car> cars = new ArrayList<>();
        cars.add(Car.builder().streetsToTravelNumber(2).streetsToTravel(Arrays.asList(streetA, streetC)).build());
        cars.add(Car.builder().streetsToTravelNumber(2).streetsToTravel(Arrays.asList(streetA, streetC)).build());
        cars.add(Car.builder().streetsToTravelNumber(2).streetsToTravel(Arrays.asList(streetA, streetC)).build());
        cars.add(Car.builder().streetsToTravelNumber(2).streetsToTravel(Arrays.asList(streetB, streetC)).build());
        cars.add(Car.builder().streetsToTravelNumber(2).streetsToTravel(Arrays.asList(streetB, streetC)).build());
        cars.add(Car.builder().streetsToTravelNumber(2).streetsToTravel(Arrays.asList(streetB, streetC)).build());

        Model model = Model.builder()
                .metadata(metadata)
                .streetsByName(streets)
                .intersectionsById(intersections)
                .cars(cars)
                .build();

        List<TrafficLightsSchedule> trafficLightsSchedules = new ArrayList<>();
        LinkedHashMap<Street, Integer> greenLightTimeSchedule = new LinkedHashMap<>();
        greenLightTimeSchedule.put(streetA, 1);
        greenLightTimeSchedule.put(streetB, 1);
        trafficLightsSchedules.add(TrafficLightsSchedule.builder()
                .intersection(middle)
                .greenLightTimeSchedule(greenLightTimeSchedule)
                .build()
        );

        Result result = Result.builder().trafficLightsSchedules(trafficLightsSchedules).build();

        //when, then
        long score = JudgingSystem.validateAndCalculateScore(result, model);
        assertEquals(65, score);

        //6 samochodów, ostatni nie kończy trasy. Mamy 10 pkt zwykłego bonusu za ukończenie, czyli w sumie 50 pkt.
        // Pierwszy samochód dostaje 5 premii za dojechanie przed czasem, każdy kolejny o 1 mniej,
        // wiec w sumie 5+4+3+2+1=15.
        // 50 za zwykłe bonusy + 15 za dojechanie przed czasem = 65
    }

}